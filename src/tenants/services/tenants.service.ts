import { Injectable } from '@nestjs/common';
import { db, auth } from '../../firebase';
import * as bcrypt from 'bcryptjs';
import { CreateTenantModel } from '../models';

const TENANT_COLLECTION = 'tenants';

@Injectable()
export class TenantsService {

    create(tenant: CreateTenantModel) {
        const model = {
            parentDomain: 'xyz.com',
            internalDomain: null,
            customDomain: null,
            activeDomain: 0,
            themeId: 1,
            setupCompleted: false,
            address1: null,
            address2: null,
            street: null,
            city: null,
            state: null,
            postalCode: null,
            country: null,
            username: null,
            password: '',
            ...tenant,
        };
        if (model.password !== '') {
            const salt = bcrypt.genSaltSync(10);
            model.password = bcrypt.hashSync(model.password, salt);
        }

        return db
            .collection(TENANT_COLLECTION)
            .doc(model.uid)
            .set(model)
            .then(() => {
                return { id: model.uid };
            });
    }

    findById(uid): Promise<any> {
        return db
            .collection(TENANT_COLLECTION)
            .doc(uid)
            .get()
            .then((snapshot) => {
                if (snapshot.exists) {
                    return {
                        uid: uid,
                        ...snapshot.data(),
                    };
                } else {
                    return { error: { message: 'Not found' } };
                }
            });
    }

    update(uid, tenant): Promise<any> {
        tenant.setupCompleted = true;
        return db
            .collection(TENANT_COLLECTION)
            .doc(uid)
            .update(tenant)
            .then(() => {
                return { success: true };
            });
    }

    setUsernameAndPassword(uid, tenant): Promise<any> {
        tenant.setupCompleted = true;
        const salt = bcrypt.genSaltSync(10);
        tenant.password = bcrypt.hashSync(tenant.password, salt);
        return db
            .collection(TENANT_COLLECTION)
            .doc(uid)
            .update(tenant)
            .then(() => {
                return { success: true };
            });
    }

    updateAuthentication(tenant): Promise<any> {
        const updateUserModel = tenant;
        return auth
            .updateUser(updateUserModel.uid, {
                email: updateUserModel.email,
                emailVerified: false,
                phoneNumber: updateUserModel.phoneNumber,
                displayName: updateUserModel.displayName,
                disabled: false,
            })
            .then(() => {
                return { success: true };
            });
    }

    getUserByPhoneNumber(phoneNumber): Promise<any> {
        return auth
            .getUserByPhoneNumber(phoneNumber)
            .then(user => {
                if (user.uid) {
                    return user;
                }

                return null;
            }, () => {
                return null;
            });
    }

    // createSurvey(uid, data): Promise<any> {
    //     return db
    //         .collection(TENANT_COLLECTION)
    //         .doc(uid)
    //         .update({ survey: data })
    //         .then((ref) => {
    //             return { message: 'Successfully' };
    //         })
    //         .catch(err => {
    //             return err;
    //         });
    // }
}
