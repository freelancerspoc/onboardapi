export class CreateTenantModel {
    readonly uid: string;
    readonly firstName: string;
    readonly lastName: number;
    readonly email: string;
    readonly phoneNumber: string;
    readonly username: string;
    readonly password: string;
}
