import { Module } from '@nestjs/common';
import { TenantsController } from './controllers';
import { TenantsService } from './services';

@Module({
    controllers: [TenantsController],
    providers: [TenantsService],
    exports: [TenantsService],
})
export class TenantsModule { }
