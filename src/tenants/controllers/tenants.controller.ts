import { Controller, Get, Req, HttpCode, Post, Param, Body, Put, Delete, InternalServerErrorException } from '@nestjs/common';
import { TenantsService } from '../services';
import { CreateTenantModel } from '../models';

@Controller('')
export class TenantsController {
    constructor(private readonly tenantService: TenantsService) { }

    @Get(':uid')
    findOne(@Param('uid') uid) {
        return this.tenantService.findById(uid);
    }

    @Post()
    @HttpCode(201)
    create(@Body() tenant: CreateTenantModel) {
        return this.tenantService.create(tenant);
    }

    @Put(':uid')
    update(@Param('uid') uid, @Body() tenant) {
        return this.tenantService.update(uid, tenant);
    }

    @Put(':uid/setUsernameAndPassword')
    setUsernameAndPassword(@Param('uid') uid, @Body() tenant) {
        return this.tenantService.setUsernameAndPassword(uid, tenant);
    }

    @Delete(':id')
    remove(@Param('id') id) {
        return `This action remove a #${id} tennant`;
    }

    @Post('/update-authentication')
    @HttpCode(204)
    updateAuthentication(@Body() tenant) {
        return this.tenantService
            .updateAuthentication(tenant)
            .catch(error => {
                throw new InternalServerErrorException(error.message);
            });
    }

    @Get('/check-existing/:phone')
    getUserByPhoneNumber(@Param('phone') phone: string) {
        return this.tenantService
            .getUserByPhoneNumber(phone)
            .catch(error => {
                throw new InternalServerErrorException(error.message);
            });
    }

    // @Post('/:uid/survey')
    // @HttpCode(204)
    // createSurvey(@Param('uid') uid, @Body() data) {
    //     return this.tenantService
    //         .createSurvey(uid, data)
    //         .catch(error => {
    //             throw new InternalServerErrorException(error.message);
    //         });
    // }
}
