import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

import { UsersController, AccountController } from './controllers';
import { UsersService, JwtStrategy, AuthService } from './services';

@Module({
    imports: [
        PassportModule.register({
            defaultStrategy: 'jwt',
            property: 'currentUser',
        }),
        JwtModule.register({
            secretOrPrivateKey: 'secretKey',
            signOptions: {
                expiresIn: 3600,
            },
        }),
    ],
    controllers: [
        AccountController,
        UsersController
    ],
    providers: [
        JwtStrategy,
        AuthService,
        UsersService,
    ],
})
export class UsersModule { }
