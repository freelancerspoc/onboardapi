import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { UsersService } from './users.service';
import { JwtPayload } from '../interfaces/jwt-payload.interface';
import { AuthToken } from '../interfaces/auth-token.interface';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly usersService: UsersService,
  ) { }

  createToken(payload: JwtPayload): AuthToken {
    const expiresIn = 3600;
    const accessToken = this.jwtService.sign(payload, { expiresIn: expiresIn });
    return {
      expiresIn: expiresIn,
      accessToken
    };
  }

  async validateUser(payload: JwtPayload): Promise<any> {
    return await this.usersService.findById(payload.sub);
  }
}
