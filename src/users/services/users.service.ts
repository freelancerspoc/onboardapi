import { Injectable } from '@nestjs/common';
import { DocumentSnapshot, QuerySnapshot } from '@google-cloud/firestore';
import * as bcrypt from 'bcryptjs';
import { db } from '../../firebase';
import { UpdateUserModel, CreateUserModel, UserCredentialModel } from '../models';

const USERS_COLLECTION = 'tenants';

@Injectable()
export class UsersService {

    find(): Promise<any> {
        return db.collection(USERS_COLLECTION)
            .get()
            .then((snapshot: QuerySnapshot) => {
                const users = [];
                snapshot.forEach(result => {
                    const doc = result.data();
                    delete doc.password;
                    users.push({
                        id: result.id,
                        ...doc
                    });
                });
                return users;
            })
            .catch(error => {
                throw new Error(error);
            });
    }
    findByUsername(username: string): Promise<any> {
        return db.collection(USERS_COLLECTION)
            .where('username', '==', username)
            .get()
            .then((snapshot: QuerySnapshot) => {
                if (snapshot.empty) {
                    return null;
                } else {
                    const users = [];
                    snapshot.forEach(result => {
                        const doc = result.data();
                        delete doc.password;
                        users.push({
                            id: result.id,
                            ...doc,
                        });
                    });
                    return {
                        username: users[0].username,
                    };
                }

            })
            .catch((error) => {
                throw new Error(error);
            });
    }

    create(model: CreateUserModel): Promise<any> {
        const data = {
            firstName: null,
            lastName: null,
            emailVerified: false,
            phoneVerified: false,
            address1: null,
            address2: null,
            street: null,
            city: null,
            state: null,
            postalCode: null,
            country: null,
            ...model,
        };
        const salt = bcrypt.genSaltSync(10);
        data.password = bcrypt.hashSync(data.password, salt);
        const collection = db.collection(USERS_COLLECTION);
        return collection.where('username', '==', data.username)
            .get()
            .then((snapshot: QuerySnapshot): Promise<any> => {
                if (snapshot.empty) {
                    return collection
                        .add(data)
                        .then((docRef) => {
                            return {
                                id: docRef.id,
                            };
                        })
                        .catch(error => {
                            throw new Error(error);
                        });
                } else {
                    throw new Error('User is exist');
                }

            })
            .catch((error) => {
                throw new Error(error);
            });

    }

    findById(id: string): Promise<any> {
        return db.collection(USERS_COLLECTION)
            .doc(id)
            .get()
            .then((snapshot: DocumentSnapshot) => {
                if (snapshot.exists) {
                    const doc = snapshot.data();
                    return {
                        id: id,
                        ...doc
                    };
                } else {
                    return null;
                }
            })
            .catch(error => {
                throw new Error(error);
            })
    }

    update(id: string, model: UpdateUserModel): Promise<any> {
        return db.collection(USERS_COLLECTION)
            .doc(id)
            .update(model)
            .then(() => {
                return { success: true };
            })
            .catch(error => {
                throw new Error(error);
            });
    }

    delete(id: string): Promise<any> {
        return db.collection(USERS_COLLECTION)
            .doc(id)
            .delete()
            .then(() => {
                return { success: true };
            })
            .catch(error => {
                throw new Error(error);
            });
    }

    changePassword(id: string, newPassword: string): any {
        const salt = bcrypt.genSaltSync(10);
        const password = bcrypt.hashSync(newPassword, salt);
        return db.collection(USERS_COLLECTION)
            .doc(id)
            .update({ password: password })
            .then(() => {
                return { success: true };
            })
            .catch(error => {
                throw new Error(error);
            });
    }

    verifyPassword(plaintextPassword: string, encrypted: string): boolean {
        return bcrypt.compareSync(plaintextPassword, encrypted);
    }

    verifyUserCredential(model: UserCredentialModel): Promise<any> {
        return db.collection(USERS_COLLECTION)
            .where('username', '==', model.username)
            .get()
            .then((snapshot: QuerySnapshot) => {
                if (snapshot.empty) {
                    return null;
                } else {
                    let user = null;
                    snapshot.forEach((doc) => {
                        const data = doc.data();
                        const passwordIsCorrect: boolean = bcrypt.compareSync(model.password, data.password);

                        user = passwordIsCorrect ? { id: doc.id, ...data } : null;
                    });

                    return user;
                }
            })
            .catch(error => {
                throw new Error(error);
            });
    }

}
