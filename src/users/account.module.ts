import { Module } from '@nestjs/common';
import { UsersService, JwtStrategy } from './services';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { AccountController } from './controllers';

@Module({
    imports: [
        PassportModule.register({
            defaultStrategy: 'jwt',
            property: 'currentUser',
        }),
        JwtModule.register({
            secretOrPrivateKey: 'secretKey',
            signOptions: {
                expiresIn: 3600,
            },
        }),
    ],
    controllers: [
        AccountController,
    ],
    providers: [
        JwtStrategy,
        UsersService,
    ],
})
export class AccountModule { }