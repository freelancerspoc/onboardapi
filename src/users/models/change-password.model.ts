export class ChangePasswordModel {
  readonly password: string;
  readonly newPassword: string;
}
