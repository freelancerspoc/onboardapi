import {
    Controller, Put, Body,
    HttpException, HttpStatus, Patch,
    UseGuards, Req, Get, UnauthorizedException, ForbiddenException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { ChangePasswordModel, ChangeEmailModel } from '../models';
import { UsersService } from '../services';

@Controller('')
@UseGuards(AuthGuard())
export class AccountController {

    constructor(
        private readonly userService: UsersService,
    ) { }

    @Get()
    getProfile(@Req() req) {
        const user = req.currentUser;

        return this.userService
            .findById(user.id)
            .then(user => {
                if (user) {
                    delete user.password;
                }
                return user;
            })
            .catch(error => {
                throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
            });
    }

    @Put('change-password')
    async changePassword(@Req() req, @Body() model: ChangePasswordModel): Promise<any> {
        const user = req.currentUser;

        if (!this.userService.verifyPassword(model.password, user.password)) {
            throw new UnauthorizedException();
        }
        return this.userService
            .changePassword(user.id, model.newPassword)
            .catch(error => {
                throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
            });
    }

    @Put('change-email')
    async changeEmail(@Req() req, @Body() model: ChangeEmailModel) {
        const user = req.currentUser;

        if (!this.userService.verifyPassword(model.password, user.password)) {
            throw new UnauthorizedException();
        }

        return this.userService
            .update(user.id, {
                email: model.email,
            })
            .catch(error => {
                throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
            });
    }

    @Patch()
    async update(@Req() req, @Body() model) {
        const user = req.currentUser;
        delete model.username;

        return this.userService
            .update(user.id, model)
            .catch(error => {
                throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
            });
    }
}
