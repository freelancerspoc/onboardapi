import { Controller, Get, Post, Put, Delete, Param, Body, Query, HttpException, HttpStatus, HttpCode, NotFoundException, Patch, ValidationPipe, BadRequestException } from '@nestjs/common';
import { UsePipes } from '@nestjs/common';

import { UsersService } from '../services';
import { CreateUserModel, UpdateUserModel } from '../models';

@Controller('')
export class UsersController {

    constructor(private readonly usersService: UsersService) {
    }

    @Get()
    async find( @Query() query) {
        return this.usersService
            .find()
            .catch(error => {
                throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
            });
    }

    @HttpCode(HttpStatus.OK)
    @Get(':id')
    async findById( @Param('id') id: string) {
        if (!id) {
            throw new BadRequestException('id is requried');
        }
        const user = await this.usersService.findById(id);
        if (user) {
            return user;
        }
        throw new NotFoundException('user not found');
    }

    @Post()
    @UsePipes(new ValidationPipe())
    async create( @Body() model: CreateUserModel) {
        return this.usersService
            .create(model)
            .catch((error) => {
                throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
            })
    }

    @Patch(':id')
    async update( @Param('id') id: string, @Body() model: UpdateUserModel) {
        if (!id) {
            throw new HttpException('Bad Request: No id.', HttpStatus.BAD_REQUEST);
        }

        return this.usersService
            .update(id, model)
            .catch(error => {
                throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
            })
    }

    @Delete(':id')
    async delete( @Param('id') id: string) {
        if (!id) {
            throw new HttpException('Bad Request: No id.', HttpStatus.BAD_REQUEST);
        }

        return this.usersService
            .delete(id)
            .catch(error => {
                throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
            })
    }

    @Get('check-existing/:username')
    async findByUsername( @Param('username') username: string): Promise<any> {
        if (!username) {
            throw new BadRequestException('username is required');
        }

        return this.usersService
            .findByUsername(username)
            .catch(error => {
                throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
            })
    }
}
