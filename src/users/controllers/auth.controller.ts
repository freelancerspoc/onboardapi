import { Controller, Post, Body, UnauthorizedException, InternalServerErrorException } from '@nestjs/common';

import { AuthService, UsersService } from '../services';
import { UserLoginModel } from '../models';

@Controller('')
export class AuthController {
    constructor(
        private readonly authService: AuthService,
        private readonly usersService: UsersService,
    ) { }

    @Post('login')
    async login( @Body() model: UserLoginModel): Promise<any> {
        return this.usersService
            .verifyUserCredential(model)
            .then(user => {
                if (user) {
                    return this.authService.createToken({ sub: user.id, username: user.username })
                }

                throw new UnauthorizedException('incorrect username or password');
            });
    }
}
