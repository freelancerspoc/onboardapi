import { Module } from '@nestjs/common';
import { AuthService, UsersService, JwtStrategy } from './services';
import { AuthController } from './controllers/auth.controller';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';

@Module({
    imports: [
        PassportModule.register({
            defaultStrategy: 'jwt',
            property: 'currentUser',
        }),
        JwtModule.register({
            secretOrPrivateKey: 'secretKey',
            signOptions: {
                expiresIn: 3600,
            },
        }),
    ],
    controllers: [
        AuthController,
    ],
    providers: [
        JwtStrategy,
        AuthService,
        UsersService,
    ],
})
export class AuthModule { }
