import { Injectable } from '@nestjs/common';
import { db } from '../../firebase';

const TENANT_COLLECTION = 'tenants';

@Injectable()
export class SurveyService {
    createSurvey(uid, data): Promise<any> {
        return db
            .collection(TENANT_COLLECTION)
            .doc(uid)
            .update({ survey: data })
            .then((ref) => {
                return { message: 'Successfully' };
            })
            .catch(err => {
                return err;
            });
    }
}
