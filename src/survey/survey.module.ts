import { Module } from '@nestjs/common';
import { SurveyController } from './controllers';
import { SurveyService } from './services';

@Module({
    controllers: [SurveyController],
    providers: [SurveyService],
    exports: [SurveyService],
})
export class SureyModule { }
