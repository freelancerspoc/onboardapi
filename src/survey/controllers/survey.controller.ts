import { Controller, Post, HttpCode, Param, Body, InternalServerErrorException } from '@nestjs/common';
import { SurveyService } from '../services';

@Controller('')

export class SurveyController {
    constructor(private readonly surveyService: SurveyService) { }

    @Post(':uid')
    @HttpCode(204)
    createSurvey(@Param('uid') uid, @Body() data) {
        return this.surveyService
            .createSurvey(uid, data)
            .catch(error => {
                throw new InternalServerErrorException(error.message);
            });
    }
}
