import * as functions from 'firebase-functions';
import { NestFactory } from '@nestjs/core';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import { ValidationPipe } from '@nestjs/common';

import { TenantsModule } from './tenants/tenants.module';
import { QuestionsModule } from './questions/questions.module';
import { SureyModule } from './survey/survey.module';
import { UsersModule } from './users/users.module';
import { AuthModule } from './users/auth.module';
import { FilesModule } from './files/files.module';
const serverTenants: express.Express = express();
const serverQuestions: express.Express = express();
const serverSurvey: express.Express = express();
const serverUsers: express.Express = express();
const serverAuth: express.Express = express();
const serverAccount: express.Express = express();
const serverFiles: express.Express = express();

async function bootstrapTenants(expressInstance) {
  const app = await NestFactory.create(TenantsModule, expressInstance);
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  await app.init();
}
bootstrapTenants(serverTenants);

exports.tenants = functions.https.onRequest(serverTenants);

async function bootstrapQuestion(expressInstance) {
  const app = await NestFactory.create(QuestionsModule, expressInstance);
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  await app.init();
}
bootstrapQuestion(serverQuestions);

exports.questions = functions.https.onRequest(serverQuestions);

async function bootstrapSurvey(expressInstance) {
  const app = await NestFactory.create(SureyModule, expressInstance);
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  await app.init();
}
bootstrapSurvey(serverSurvey);

exports.survey = functions.https.onRequest(serverSurvey);

async function bootstrapUsers(expressInstance) {
  const app = await NestFactory.create(UsersModule, expressInstance);
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  await app.init();
}
bootstrapUsers(serverUsers);

exports.users = functions.https.onRequest(serverUsers);

async function bootstrapAuth(expressInstance) {
  const app = await NestFactory.create(AuthModule, expressInstance);
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  await app.init();
}
bootstrapAuth(serverAuth);

exports.auth = functions.https.onRequest(serverAuth);

async function bootstrapAccount(expressInstance) {
  const app = await NestFactory.create(UsersModule, expressInstance);
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  await app.init();
}
bootstrapAccount(serverAccount);

exports.account = functions.https.onRequest(serverAccount);

async function bootstrapFiles(expressInstance) {
  const app = await NestFactory.create(FilesModule, expressInstance);
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  await app.init();
}
bootstrapFiles(serverFiles);

exports.files = functions.https.onRequest(serverFiles);
