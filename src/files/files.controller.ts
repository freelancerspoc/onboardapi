import { Controller, Post, Req, Res, Get } from '@nestjs/common';
const Busboy = require('busboy');
import { bucket } from '../firebase';

@Controller('')
export class FilesController {

  @Post('upload')
  uploadedFile(@Req() req, @Res() res) {
    const busboy = new Busboy({ headers: req.headers });
    let filePromise = null;

    busboy.on('file', (fieldname, file, filename) => {

      filePromise = new Promise((resolve, reject) => {

        file.on('data', (data) => {
          const gcsname = Date.now() + filename;
          const file = bucket.file(gcsname);

          const stream = file.createWriteStream({
            resumable: false,
          });

          stream.on('finish', () => {
            file.makePublic().then(() => {
              resolve({
                reference: `${gcsname}`,
              });
            });
          });

          stream.end(data);
        });

      });
    });

    busboy.on('finish', () => {

      if (!filePromise) {
        return res.status(500).end();
      }
      filePromise.then((data) => {
        return res.json(data);
      }, (error) => {
        return res.status(500).end(error);
      });
    });

    if (req.rawBody) {
      busboy.end(req.rawBody);
    } else {
      req.pipe(busboy);
    }

  }
}
