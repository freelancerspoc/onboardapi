import { Module } from '@nestjs/common';
import { QuestionsController } from './controllers';
import { QuestionsService } from './services';

@Module({
    controllers: [QuestionsController],
    providers: [QuestionsService],
    exports: [QuestionsService],
})
export class QuestionsModule { }
