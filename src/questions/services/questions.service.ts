import { Injectable } from '@nestjs/common';

import { QuerySnapshot } from '@google-cloud/firestore';
import { db } from '../../firebase';
const QUESTION_COLLECTION = 'questions';

@Injectable()
export class QuestionsService {
    createQuestionnaire(data) {
        return db
            .collection(QUESTION_COLLECTION)
            .doc()
            .set(data)
            .then((ref) => {
                return { message: 'Successfully' };
            })
            .catch(err => {
                return err;
            });
    }

    getQuestionnaire() {
        return db.collection(QUESTION_COLLECTION)
            .get()
            .then((snapshot: QuerySnapshot) => {
                const questions = [];
                snapshot.forEach(result => {
                    const doc = result.data();
                    questions.push({
                        ...doc,
                    });
                });
                return questions;
            })
            .catch(error => {
                throw new Error(error);
            });
    }
}
