import { Controller, Get, Req, HttpCode, Post, Param, Body, Put, Delete } from '@nestjs/common';
import { QuestionsService } from '../services';

@Controller('')

export class QuestionsController {
    constructor(private readonly questionsService: QuestionsService) { }
    /** POST api/questions - Create questionnaire */
    @Post()
    @HttpCode(204)
    createQuestionnaire( @Body() data) {
        return this.questionsService.createQuestionnaire(data);
    }

    /** GET api/questions - Get questionnaire */
    @Get()
    getQuestionnaire() {
        return this.questionsService.getQuestionnaire();
    }
}
