import { NestFactory } from '@nestjs/core';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import { ValidationPipe } from '@nestjs/common';

import { TenantsModule } from './tenants/tenants.module';
import { QuestionsModule } from './questions/questions.module';
import { SureyModule } from './survey/survey.module';
const serverTenants: express.Express = express();
const serverQuestions: express.Express = express();
const serverSurvey: express.Express = express();

async function bootstrapTenants(expressInstance) {
  const app = await NestFactory.create(TenantsModule, expressInstance);
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  await app.init();
}
bootstrapTenants(serverTenants);

async function bootstrapQuestion(expressInstance) {
  const app = await NestFactory.create(QuestionsModule, expressInstance);
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  await app.init();
}
bootstrapQuestion(serverQuestions);

async function bootstrapSurvey(expressInstance) {
  const app = await NestFactory.create(SureyModule, expressInstance);
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  await app.init();
}
bootstrapSurvey(serverSurvey);
