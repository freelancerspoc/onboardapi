import { Controller, Get } from '@nestjs/common';

@Controller()
export class AppController {
    @Get('welcome')
    root(): string {
        return 'Welcome!';
    }
}
