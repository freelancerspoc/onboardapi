import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { TenantsModule } from './tenants/tenants.module';
import { QuestionsModule } from './questions/questions.module';
import { SureyModule } from './survey/survey.module';
import { UsersModule } from './users/users.module';
import { AuthModule } from './users/auth.module';
import { FilesModule } from './files/files.module';

@Module({
  imports: [
    TenantsModule,
    QuestionsModule,
    SureyModule,
    UsersModule,
    AuthModule,
    FilesModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {

}
